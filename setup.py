#!/usr/bin/env python

# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 3
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; If not, see <http://www.gnu.org/licenses/>.
#
# ##### END GPL LICENSE BLOCK #####


__author__ = "Sergi Blanch-Torne"
__email__ = "sblanch@cells.es"
__copyright__ = "Copyright 2021, CELLS / ALBA Synchrotron"
__license__ = "GPLv3+"

__project__ = 'struck'
__description__ = "Python module to access the struck cards"
__longDesc__ = """
This module has been developed to provide access to the struck card SIS8300-KU
used at the Alba Synchrotron.
"""
__url__ = "https://git.cells.es/Controls/python-struck"
# we use semantic versioning (http://semver.org/) and we update it using the
# bumpversion script (https://github.com/peritus/bumpversion)
__version__ = '1.0.0'


from setuptools import setup, find_packages

# no alphabetical order, must be after setuptools
from build_cython import build_extension, cmdclass


classifiers = [
    'Development Status :: 1 - Planning',
    'Intended Audience :: Developers',
    'Intended Audience :: Science/Research',
    'License :: OSI Approved :: '
    'GNU General Public License v3 or later (GPLv3+)',
    'Operating System :: POSIX',
    'Programming Language :: Python :: 3.9',
    'Programming Language :: Cython',
    'Topic :: Scientific/Engineering :: Interface Engine/Protocol Translator',
    'Topic :: Software Development :: Embedded Systems',
    'Topic :: Software Development :: Libraries :: Python Modules',
    ]


setup(name=__project__,
      license=__license__,
      description=__description__,
      long_description=__longDesc__,
      version=__version__,
      author=__author__,
      author_email=__email__,
      classifiers=classifiers,
      packages=find_packages(),
      ext_modules=build_extension(),
      cmdclass=cmdclass,
      url=__url__,
      )

# for the classifiers review see:
# https://pypi.python.org/pypi?%3Aaction=list_classifiers
#
# Development Status :: 1 - Planning
# Development Status :: 2 - Pre-Alpha
# Development Status :: 3 - Alpha
# Development Status :: 4 - Beta
# Development Status :: 5 - Production/Stable
