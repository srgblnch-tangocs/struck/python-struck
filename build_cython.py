#!/usr/bin/env python

# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 3
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; If not, see <http://www.gnu.org/licenses/>.
#
# ##### END GPL LICENSE BLOCK #####


from Cython.Build import cythonize
import os
from setuptools import Command


def find_file_by_extension(path='.', ext='.pyx'):
    files = []
    for root, dirs, filenames in os.walk(path):
        for filename in filenames:
            if filename.endswith(ext):
                files.append(os.path.join(root, filename))
    return files


def find_directories(path='.', names=None):
    directories = []
    if names is not None and isinstance(names, list):
        for root, directories_list, file_names in os.walk(path):
            if len(directories_list) > 0:
                for directory in directories_list:
                    if directory in names:
                        directory_name = f"{root}/{directory}"
                        if directory_name not in directories:
                            directories.append(directory_name)
    return directories


def find_pyx(path='.'):
    return find_file_by_extension(path, ext='.pyx')


def find_so(path='.'):
    return find_file_by_extension(path, ext='.so')


def is_submodule(file_name):
    path_elements = file_name.split('/', 2)
    if path_elements[0] != '.':
        raise AssertionError(f"file name not understood")
    if path_elements[1] != "struck":
        raise AssertionError(f"file name not under the main module")
    if path_elements[2].count('/'):
        return True
    return False


def get_submodule_name(file_name):
    return file_name.split('/', 2)[2].rsplit('/', 1)[0]


# FIXME: this library and heaers aren't installed and the path is hardcoded
submodules = {'sis830x': {
    'include': ['/home/alba/SIS/lib/libSIS830x/sis830x.h',
                '/home/alba/SIS/lib/libSIS830x/sis830xStat.h',
                '/home/alba/SIS/lib/libSIS830x/sis830xType.h'],
    'lib': ['SIS830x'],
    'library_dirs': ['/home/alba/SIS/lib/libSIS830x']}}


def append_include(source, submodule):
    if submodule in submodules and 'include' in submodules[submodule]:
        source.include_dirs += submodules[submodule]['include']


def append_library(source, submodule):
    if submodule in submodules:
        if 'lib' in submodules[submodule]:
            source.libraries += submodules[submodule]['lib']
        if 'library_dirs' in submodules[submodule]:
            source.library_dirs += submodules[submodule]['library_dirs']


def build_extension():
    extensions = []
    files = find_pyx()
    for file in files:
        sources = cythonize(file)
        if is_submodule(file):
            submodule = get_submodule_name(file)
            for source in sources:  # TODO: improve. many failure points
                append_include(source, submodule)
                append_library(source, submodule)
        extensions += sources
    for extension in extensions:
        print(f"\n{extension.__dict__}\n")
    return extensions


class CleanCommand(Command):
    """Custom clean command to tidy up the project root."""
    user_options = []

    def initialize_options(self):
        pass

    def finalize_options(self):
        pass

    def run(self):
        sources = ['./build', './*.egg-info']
        files = []
        for file_name in find_pyx():
            for extension in ['.c', '.cpp']:
                candidate = file_name.replace('.pyx', extension)
                if os.path.exists(candidate):
                    files.append(candidate)
        for so_file in find_so():
            files.append(so_file)
        for pyc in find_file_by_extension(ext='.pyc'):
            files.append(pyc)
        for file in files:
            os.system(f"rm -vrf {file}")
        directories = find_directories(
            names=['__pycache__', 'build', 'dist', 'struck.egg-info'])
        for directory in directories:
            os.system(f"rm -vrf {directory}")


cmdclass = {}
cmdclass.update({'clean': CleanCommand})
