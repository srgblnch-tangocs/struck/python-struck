# python-struck

![5 - Production](https://img.shields.io/badge/Development_Status-5_--_production-green.svg)

Licence: GPLv3+

release 1.0.0

## Conda prerequisite
```bash
apt-get install curl
CONDA_RELEASE=2020.11
OS=`uname -s`
ARCH=`uname -m`
curl –O https://repo.anaconda.com/archive/Anaconda3-$CONDA_RELEASE-$OS-$ARCH.sh > Anaconda3-$CONDA_RELEASE-$OS-$ARCH.sh

MD5SUM=4cd48ef23a075e8555a8b6d0a8c4bae2
echo "$MD5SUM Anaconda3-$CONDA_RELEASE-$OS-$ARCH.sh" | md5sum --check  -

bash Anaconda3-$CONDA_RELEASE-$OS-$ARCH.sh
```

Then one can logout/login or call `source ~/.bashrc` and check `conda info`.

## Prepare the conda environment
```bash
conda env create -f environment.yml
conda activate python-struck
```
or if it already exist
```bash
conda env update -f environment.yml
conda activate python-struck
```

```bash
python setup.py install
export LD_LIBRARY_PATH=/home/alba/SIS/lib/libSIS830x:$LD_LIBRARY_PATH
```

### Build a Python Wheel

One can install in the current environment or left it prepared to be 
installed in another environment in this system (or another with the same 
arch, remember the cython usage but don't set it as dependency where this 
module is used).

```bash
python setup.py bdist_wheel
```

This will generate a file ```dist/struck-0.1.0-cp39-cp39-linux_x86_64.
whl``` that can be installed when you are in another python environment.

As an example from a project above that uses this module [taods-struck](https://git.cells.es/controls/struck/tangods-struck):
```bash
conda activate tangods-struck
pip install dist/struck-1.0.0-cp39-cp39-linux_x86_64.whl
```

## Accessing the card

```python
>>> import struck
>>> struck.version  # version of the python module
 '1.0.0'
>>> import struck.sis830x
>>> struck.sis830x.version()  # version of the struck library
 (1, 3)
>>> struck.sis830x.get_number_of_devices()
 1
```

There is a sample code in SIS:
```
$ ~/SIS/universal_tests/register/regread /dev/sis8300-0 0
reg = 0x83032811
```

We can replicate in python having the same results:
```python
>>> import struck.sis830x
>>> reg_value = struck.sis830x.regread("/dev/sis8300-0", 0)
>>> reg_value, hex(reg_value)
 (2198022161, '0x83032811')
```
The message `reg 0 = 0x83032811` is a print in the code. The returning value 
is the decimal representation of the same value given by the sample code.

This can be called using bytes:
```python
>>> import struck.sis830x
>>> reg_value = struck.sis830x.regread(b"/dev/sis8300-0", 0)
>>> reg_value, hex(reg_value)
 (2198022161, '0x83032811')
```

Or by the index (as we only have one card, the index is 0)
```python
>>> import struck.sis830x
>>> reg_value = struck.sis830x.regread(0, 0)
>>> reg_value, hex(reg_value)
 (2198022161, '0x83032811')
```

### Multiple registers read

Instead of open and close the access to the card on each read, use an object:
```python
>>> import struck.sis830x
>>> card = struck.sis830x.Card(b"/dev/sis8300-0")  # or use the index
>>> card.open()
>>> reg_value = card.regread(0)
>>> reg_value, hex(reg_value)
 (2198022161, '0x83032811')
>>> card.close()
```

Even better use `with`:
```python
>>> import struck.sis830x
>>> with struck.sis830x.Card(b"/dev/sis8300-0") as card:
...     reg_value = card.regread(0)
>>> reg_value, hex(reg_value)
 (2198022161, '0x83032811')
```
