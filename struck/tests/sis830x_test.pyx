#!/usr/bin/env python

# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 3
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; If not, see <http://www.gnu.org/licenses/>.
#
# ##### END GPL LICENSE BLOCK #####

# This module is not tested directly from pytest, is the cython_modules_test.py
# file the one that imports this module if it has been cythonized:
# $ cythonize -i struck/tests/sis830x_test.pyx
# Also the cytest module provides a decorator to make the function callable

from .cytest import cytest
import pytest


# @pytest.mark.parametrize("file_name", ['/dev/sis8300-0'])
@cytest
def test_create_card_object():
#    cimport struck.sis830x
#    card = struck.sis830x.Card()
#    assert card is not None
    assert True
