#!/usr/bin/env python

# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 3
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; If not, see <http://www.gnu.org/licenses/>.
#
# ##### END GPL LICENSE BLOCK #####

# Further info in: https://shwina.github.io/cython-testing/

import importlib
import sys


# ########
# prepare with: (any other cython test shall be in this list
#                and requires the command cythonize)
# cythonize -i struck/tests/sis830x_test.pyx
cython_test_modules = ["sis830x_test"]

# print(f"__name__: {__name__}")
print(f"file name: {__file__}")
for module_name in cython_test_modules:
    print(f"for module: {module_name}")
    try:
        module_obj = importlib.import_module(module_name)
        print(f"import {module_obj}")
        for attribute_name in dir(module_obj):
            item = getattr(module_obj, attribute_name)
            if callable(item) and attribute_name.startswith("test_"):
                print(f"\t'{item.__name__}' is callable and it is recognized as a test")
                setattr(sys.modules[__name__], attribute_name, item)
    except ImportError as exc:
        print(f"Exception: {exc}")  # pass
