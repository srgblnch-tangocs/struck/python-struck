# !/usr/bin/env python

# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 3
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; If not, see <http://www.gnu.org/licenses/>.
#
# ##### END GPL LICENSE BLOCK #####


class Code:
    _code = None
    _valid_values = None
    _valid_strings = None

    def __init__(self, code: int):
        self._set_value(code)

    def __int__(self):
        return self._code

    def __str__(self):
        return self.meaning

    def __repr__(self):
        return f"{self._code}:{self.meaning}"

    def __richcmp__(self, other, op: int):
        if op in [2, 3]:  # ==, !=
            if int(self) == int(other):
                return True if op == 2 else False
            return False if op == 2 else True
        return False

    @property
    def code(self):
        return self._code

    @code.setter
    def code(self, value):
        self._set_value(value)

    def _set_value(self, value):
        if value in self.valid_codes():
            self._code = value
        else:
            raise ValueError("Invalid value to assign")

    @property
    def meaning(self):
        str_code, str_help = self._interpreter(self._code)
        return f"[{str_code}] {str_help!s}"

    @property
    def string(self):
        str_code, _ = self._interpreter(self._code)
        return f"{str_code}"

    @property
    def human(self):
        _, str_help = self._interpreter(self._code)
        return f"{str_help}"

    def _interpreter(self, value):
        if self._valid_values is None:
            raise NotImplementedError("Not implemented in abstract superclass!")
        return self._valid_values.get(
            value, [None, f"Unknown {self.__class__.__name__} ({self._code})"])

    def valid_values(self):
        if self._valid_values is None:
            raise NotImplementedError("Not implemented in abstract superclass!")
        return self._valid_values

    def valid_codes(self):
        if isinstance(self._valid_values, dict):
            return list(self._valid_values.keys())
        return []

    def valid_strings(self):
        if self._valid_strings is None:
            self._valid_strings = []
            for code in self._valid_values.keys():
                self._valid_strings.append(self._valid_values[code][0])
        return self._valid_strings[:]
