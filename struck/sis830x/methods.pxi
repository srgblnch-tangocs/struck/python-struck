# !/usr/bin/env python

# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 3
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; If not, see <http://www.gnu.org/licenses/>.
#
# ##### END GPL LICENSE BLOCK #####


include "../basictypes.pxi"


from libcpp cimport bool as bool_t
from libc.stdlib cimport malloc, free
from libc.string cimport strcpy, strlen

cdef extern from "<stdbool.h>" nogil:
    pass


# FIXME: this library and heaers aren't installed and the path is hardcoded
cdef extern from "/home/alba/SIS/lib/libSIS830x/sis830xType.h" nogil:
    ctypedef enum moduleType:
        UNKNOWN
        SIS8300
        SIS8300L
        SIS8300L2
        SIS8325
        SIS8800
        SIS8300KU
    ctypedef struct SIS830X_VERSION:
        int major
        int minor
    ctypedef SIS830X_VERSION *PSIS830X_VERSION
    ctypedef struct SIS830X_DEVICE:
        bool_t open
        int fp
        moduleType type
        uint32_t ident_version
        uint32_t serial_number
    ctypedef SIS830X_DEVICE *PSIS830X_DEVICE
    ctypedef struct si5338a_Reg_Data:
        unsigned char Reg_Addr
        unsigned char Reg_Val
        unsigned char Reg_Mask


# FIXME: this library and heaers aren't installed and the path is hardcoded
cdef extern from "/home/alba/SIS/lib/libSIS830x/sis830xStat.h" nogil:
    ctypedef enum SIS830X_STATUS:
        Stat830xSuccess
        Stat830xInvalidDeviceIndex
        Stat830xNullArgument
        Stat830xAlreadyOpen
        Stat830xNotOpen
        Stat830xIoctlError
        Stat830xReadCallError
        Stat830xReadLenError
        Stat830xWriteCallError
        Stat830xWriteLenError
        Stat830xSpiBusy
        Stat830xFlashBusy
        Stat830xInvalidPath
        Stat830xFileTooLarge
        Stat830xMemAlloc
        Stat830xNotSupported
        Stat830xVerifyError
        Stat830xI2cBusy
        Stat830xI2cNack
        Stat830xSynthNoLock
        Stat830xFileNotSupported
        Stat830xFlashEraseError
        Stat830xClkMuxCalibError
        Stat830xClkMuxParaBwSelErr
        Stat830xClkMuxParaN1HsErr
        Stat830xClkMuxParaNc1LsErr
        Stat830xClkMuxParaNc2LsErr
        Stat830xClkMuxParaN2HsErr
        Stat830xClkMuxParaN2LsErr
        Stat830xClkMuxParaN31Err
        Stat830xClkMuxParaClkInFreqErr


# FIXME: this library and heaers aren't installed and the path is hardcoded
cdef extern from "/home/alba/SIS/lib/libSIS830x/sis830x.h" nogil:
    SIS830X_STATUS sis830x_Version(PSIS830X_VERSION version)
    SIS830X_STATUS sis830x_GetNumberOfDevices(int *num)
    SIS830X_STATUS sis830x_OpenDeviceOnIdx(int idx, PSIS830X_DEVICE device)
    SIS830X_STATUS sis830x_OpenDeviceOnPath(char *path, PSIS830X_DEVICE device)
    SIS830X_STATUS sis830x_ReadRegister(PSIS830X_DEVICE device, uint32_t addr,
                                        uint32_t *data)
    SIS830X_STATUS sis830x_WriteRegister(PSIS830X_DEVICE device, uint32_t addr,
                                         uint32_t data)
    SIS830X_STATUS sis830x_ReadMemory(PSIS830X_DEVICE device, uint32_t addr,
                                      uint32_t *data, uint32_t len)
    SIS830X_STATUS sis830x_CloseDevice(PSIS830X_DEVICE device)
