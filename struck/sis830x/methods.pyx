# !/usr/bin/env python

# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 3
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; If not, see <http://www.gnu.org/licenses/>.
#
# ##### END GPL LICENSE BLOCK #####

cimport numpy
import numpy
from .structures import Sis830xStatus


numpy.import_array()


def version():
    """
Request to the struck api the version of the sis830X
    """
    cdef:
        PSIS830X_VERSION version
    version = <PSIS830X_VERSION> malloc(1 * sizeof(PSIS830X_VERSION))
    try:
        error = Sis830xStatus(sis830x_Version(version))
        if int(error) != 0:
            raise AssertionError(f"Error code returned '{error!r}'")
        major, minor = version.major, version.minor
    except Exception:  # to make sure the close will be called.
        pass
    free(version)
    return (major, minor)


def get_number_of_devices():
    """
Request to the struck API the number of 830X devices in the system
    """
    cdef:
        int number_of_devices
    ret_status = Sis830xStatus(
        sis830x_GetNumberOfDevices(&number_of_devices))
    if int(ret_status) != 0:
        raise AssertionError(f"Error getting the number of devices. "
                             f"Code returned '{ret_status!r}'")
    return number_of_devices


def regread(device_or_index, _offset):
    """
This method is used to request the value on an specific register.

It can be called specifying the /dev/sis* char file in the first argument or
the index of the card that likes to be accessed
    """
    cdef:
        unsigned int offset, data
        SIS830X_DEVICE fp
        PSIS830X_DEVICE device
        int number_of_devices
        int index
        char *device_name

    fp.open = False
    device = &fp
    offset = _offset

    if isinstance(device_or_index, int):
        index = device_or_index

        ret_status = Sis830xStatus(
            sis830x_GetNumberOfDevices(&number_of_devices))
        if int(ret_status) != 0:
            raise AssertionError(f"Error getting the number of devices. Code"
                                 f"returned '{ret_status!r}'")
        if index >= number_of_devices:
            raise IndexError(f"There are only {number_of_devices}. "
                             f"Indexes start at 0 so it cannot open number "
                             f"{index}")
        ret_status = Sis830xStatus(sis830x_OpenDeviceOnIdx(index, device))
        if int(ret_status) != 0:
            raise AssertionError(f"Error in open. "
                                 f"Code returned '{ret_status!r}'")
    elif isinstance(device_or_index, str) or \
            isinstance(device_or_index, bytes):
        if isinstance(device_or_index, str):
            device_or_index = device_or_index.encode()
        device_name = device_or_index
        ret_status = Sis830xStatus(
            sis830x_OpenDeviceOnPath(device_name, device))
        if int(ret_status) != 0:
            raise AssertionError(f"Error in open. "
                                 f"Code returned '{ret_status!r}'")
    else:
        raise TypeError(f"Unrecognized type for the first parameter")

    try:
        ret_status = Sis830xStatus(
            sis830x_ReadRegister(device, offset, &data))
        if int(ret_status) != 0:
            raise AssertionError(f"Error in read. "
                                 f"Code returned '{ret_status!r}'")

        # print(f"reg {offset} = 0x{data:X}")
    except:
        pass  # to make sure the close will be called.

    ret_status = Sis830xStatus(sis830x_CloseDevice(device))
    if int(ret_status) != 0:
            raise AssertionError(f"Error in close. "
                                 f"Code returned '{ret_status!r}'")

    return data

cdef class Card:
    """
Object that represents a struck sis830X card.

The use of this object allows to open once the link with the card to do many
read/write operations. When finish, call to close() or it will called when the
object is destroyed.

    """
    def __init__(self, device_or_index, last_register=0xfff):
        """
To build the Card object it is necessary to name the card to be used. It can
be referenced by the path to the character file in the /dev or the number
of the card in the system.

>>> struck.sis830x.get_number_of_devices()
1
>>> struck.sis830x.Card(0)
or
>>> struck.sis830x.Card("/dev/sis8300-0")
        """
        if isinstance(device_or_index, int):
            self.index = device_or_index
            number_of_devices = get_number_of_devices()
            if number_of_devices == 0:
                raise IndexError(f"No cards present in the system.")
            elif self.index >= number_of_devices:
                raise IndexError(f"There are only {number_of_devices}. "
                                 f"Indexes start at 0 so it cannot open number "
                                 f"{self.index}")
        elif isinstance(device_or_index, str) or \
            isinstance(device_or_index, bytes):
            if isinstance(device_or_index, str):
                device_or_index = device_or_index.encode()
            self.index = -1
            self.device_name = device_or_index
        else:
            raise TypeError(f"Unrecognized type for the first parameter")
        self.fp.open = False
        self.register_upper_bound = last_register

    def __enter__(self):
        self.open()
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.close()

    def __del__(self):
        self.close()

    def __str__(self):
        if self.index == -1:
            return f"{self.device_name}"
        else:
            return f"sis8300-{self.index}"

    def __repr__(self):
        return self.__str__()

    def open(self):
        """
Open the link to the card. Necessary to any read/write operation.

>>> card.open()
        """
        cdef:
            PSIS830X_DEVICE device
            char *device_name_as_char
        device = &self.fp
        if self.index >= 0:
            ret_status = Sis830xStatus(
                sis830x_OpenDeviceOnIdx(self.index, device))
            if int(ret_status) == 3:  # Stat830xAlreadyOpen
                pass
            elif int(ret_status) != 0:
                raise AssertionError(f"Error in open. "
                                     f"Code returned '{ret_status!r}'")
        else:
            device_name_as_char = self.device_name
            ret_status = Sis830xStatus(
                sis830x_OpenDeviceOnPath(device_name_as_char, device))
            if int(ret_status) == 3:  # Stat830xAlreadyOpen
                pass
            elif int(ret_status) != 0:
                raise AssertionError(f"Error in open. "
                                     f"Code returned '{ret_status!r}'")

    def close(self):
        """
Close the link to the card. When there are no more read/write operation.

>>> card.close()
        """
        cdef:
            PSIS830X_DEVICE device
        device = &self.fp
        ret_status = Sis830xStatus(sis830x_CloseDevice(device))
        if int(ret_status) == 4:  # Stat830xNotOpen
            pass
        elif int(ret_status) != 0:
            raise AssertionError(f"Error in close. "
                                 f"Code returned '{ret_status!r}'")

    # TODO: remove when documentation doesn't reference it
    def regread(self, _offset):
        """
Deprecated method to read a register. Use read_register()
        """
        return self.read_register(_offset)

    def read_register(self, _offset):
        """
Method to, with an open link to the card, read the register the number of which
is passed as an argument.

>>> value = card.read_register(0x400)
        """
        cdef:
            unsigned int offset, data
            PSIS830X_DEVICE device
        if _offset > self.register_upper_bound:
            raise IndexError(f"Register {_offset} is above the limit "
                             f"{self.register_upper_bound}")
        device = &self.fp
        offset = _offset
        ret_status = Sis830xStatus(
            sis830x_ReadRegister(device, offset, &data))
        if int(ret_status) != 0:
            raise AssertionError(f"Error in read. "
                                 f"Code returned '{ret_status!r}'")
        # print(f"reg 0x{offset:X} -> 0x{data:X}")
        return data

    def write_register(self, _offset, _data, confirmation=True):
        """
Method to, with an open link to the card, write the register the number of
which is passed as first argument, and the value as the second argument.

There is a third argument, optional, called confirmation, that by default
it is True, that provides a read after write and returns the value from
the card.

>>> readback = card.write_register(0x400, value)
>>> card.write_register(0x400, value, confirmation=False)
        """
        cdef:
            unsigned int offset, data
            PSIS830X_DEVICE device
        if _offset > self.register_upper_bound:
            raise IndexError(f"Register {_offset} is above the limit "
                             f"{self.register_upper_bound}")
        device = &self.fp
        offset = _offset
        data = _data
        # print(f"reg 0x{offset:X} <- 0x{data:X}")
        ret_status = Sis830xStatus(
            sis830x_WriteRegister(device, offset, data))
        if int(ret_status) != 0:
            raise AssertionError(f"Error in write. "
                                 f"Code returned '{ret_status!r}'")
        if confirmation:
            return self.read_register(_offset)

    def read_memory(self, first_register, how_many):
        """
Method to, with an open link to the card, read a block of the memory. First
argument is the initial position of the read, and the second is the number of
registers after that to be read. This returns a numpy array.

>>> values = card.read_memory(0, 10)
        """
        cdef:
            uint32_t addr
            uint32_t *data
            uint32_t len
            PSIS830X_DEVICE device
            numpy.ndarray[numpy.npy_intp, ndim=1, mode="c"] shape
        # print(f"read_memory({first_register}, {how_many})")
        device = &self.fp
        addr = first_register
        data = <uint32_t*> malloc(how_many*sizeof(uint32_t))
        if not data:
            raise MemoryError("Error in memory allocation to read")
        len = how_many
        try:
            ret_status = Sis830xStatus(
                sis830x_ReadMemory(device, addr, data, len))
            if int(ret_status) != 0:
                raise AssertionError(f"Error in read. "
                                     f"Code returned '{ret_status!r}'")
            dimensions = 1
            shape = numpy.array((how_many,))
            array = numpy.array(numpy.PyArray_SimpleNewFromData(
                                dimensions, &shape[0], numpy.NPY_UINT32,
                                <void*> data), copy=True)
        except Exception as exception:
            print(f"exception in read_memory: {exception}")
            pass
        free(data)
        return array

    def read_registers(self, *registers):
        """
Given a list of registers, read them as much concurrent as possible and return
a list with the values.

>>> values_list = card.read_registers(0x400, 0x401, 0x600)
        """
        cdef:
            unsigned int offset, data
            PSIS830X_DEVICE device
        device = &self.fp
        values = []
        for register in registers:
            if register > self.register_upper_bound:
                raise IndexError(f"Register {register} is above the limit "
                                 f"{self.register_upper_bound}")
            offset = register
            ret_status = Sis830xStatus(
                sis830x_ReadRegister(device, offset, &data))
            if int(ret_status) != 0:
                raise AssertionError(f"Error in read. "
                                     f"Code returned '{ret_status!r}'")
            # print(f"reg 0x{offset:X} -> 0x{data:X}")
            values.append(data)
        return values

    def write_registers(self, *pairs, confirmation=True):
        """
Given a list of pairs of registers and values, write them as much concurrent as
possible.

There is a second argument, optional, called confirmation, that by default
it is True, that provides a read after write and returns the values from
the card.

>>> readback_list = card.write_register((0x400, 1), (0x401, 1))
>>> card.write_register((0x400, 1), (0x401, 1), confirmation=False)
        """
        cdef:
            unsigned int offset, data
            PSIS830X_DEVICE device
        device = &self.fp
        for register, value in pairs:
            if register > self.register_upper_bound:
                raise IndexError(f"Register {register} is above the limit "
                                 f"{self.register_upper_bound}")
            offset = register
            data = value
            # print(f"reg 0x{offset:X} <- 0x{data:X}")
            ret_status = Sis830xStatus(
                sis830x_WriteRegister(device, offset, data))
            if int(ret_status) != 0:
                raise AssertionError(f"Error in write. "
                                     f"Code returned '{ret_status!r}'")
        if confirmation:
            registers = [register for register, value in pairs]
            return self.read_registers(*registers)
