# !/usr/bin/env python

# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 3
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; If not, see <http://www.gnu.org/licenses/>.
#
# ##### END GPL LICENSE BLOCK #####


include "../basictypes.pxi"


# FIXME: this library and heaers aren't installed and the path is hardcoded
cdef extern from "/home/alba/SIS/lib/libSIS830x/sis830xStat.h" nogil:
    ctypedef enum SIS830X_STATUS: pass

# FIXME: this library and heaers aren't installed and the path is hardcoded
cdef extern from "/home/alba/SIS/lib/libSIS830x/sis830x.h" nogil:
    void sis830x_status2str(SIS830X_STATUS status_in, char *text_out)