# !/usr/bin/env python

# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 3
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; If not, see <http://www.gnu.org/licenses/>.
#
# ##### END GPL LICENSE BLOCK #####


from ctypes import Structure, c_bool, c_ubyte, c_int, c_uint32
from libc.string cimport strcpy, strlen

from ..code import Code

# ------------------------------------------------------------------------------
# from lib/libSIS830x/sis830xStat.h
# ------------------------------------------------------------------------------
API_RETURNCODE_START = 0

Stat830xSuccess = API_RETURNCODE_START
Stat830xInvalidDeviceIndex = 1
Stat830xNullArgument = 2
Stat830xAlreadyOpen = 3
Stat830xNotOpen = 4
Stat830xIoctlError = 5
Stat830xReadCallError = 6
Stat830xReadLenError = 7
Stat830xWriteCallError = 8
Stat830xWriteLenError = 9
Stat830xSpiBusy = 10
Stat830xFlashBusy = 11
Stat830xInvalidPath = 12
Stat830xFileTooLarge = 13
Stat830xMemAlloc = 14
Stat830xNotSupported = 15
Stat830xVerifyError = 16
Stat830xI2cBusy = 17
Stat830xI2cNack = 18
Stat830xSynthNoLock = 19
Stat830xFileNotSupported = 20
Stat830xFlashEraseError = 21
Stat830xClkMuxCalibError = 22
Stat830xClkMuxParaBwSelErr = 23
Stat830xClkMuxParaN1HsErr = 24
Stat830xClkMuxParaNc1LsErr = 25
Stat830xClkMuxParaNc2LsErr = 26
Stat830xClkMuxParaN2HsErr = 27
Stat830xClkMuxParaN2LsErr = 28
Stat830xClkMuxParaN31Err = 29
Stat830xClkMuxParaClkInFreqErr = 30


class Sis830xStatus(Code):
    """
interpreter of the returning codes of the lower level api

>>> code = struck.sis830x.structures.Sis830xStatus(0)
>>> # composition of the value, the constant string and the interpretation
>>> repr(code)
 '0:[NoError] no error'
>>> int(code)  # integer value of the error code
 0
>>> int(code) == 0  # useful for comparisons
 True
>>> code.string  # constant string from the api
 'NoError'
>>> code.human  # error description for humans
 'no error'
>>> code.meaning  # combination of the constant string and the human description
 '[NoError] no error'
    """
    _valid_values = {
        Stat830xSuccess: ["NoError"],
        Stat830xInvalidDeviceIndex: ["InvalidDeviceIndex"],
        Stat830xNullArgument: ["NullArgument"],
        Stat830xAlreadyOpen: ["AlreadyOpen"],
        Stat830xNotOpen: ["NotOpen"],
        Stat830xIoctlError: ["IoctlError"],
        Stat830xReadCallError: ["ReadCallError"],
        Stat830xReadLenError: ["ReadLenError"],
        Stat830xWriteCallError: ["WriteCallError"],
        Stat830xWriteLenError: ["WriteLenError"],
        Stat830xSpiBusy: ["SpiBusy"],
        Stat830xFlashBusy: ["FlashBusy"],
        Stat830xInvalidPath: ["InvalidPath"],
        Stat830xFileTooLarge: ["FileTooLarge"],
        Stat830xMemAlloc: ["MemAlloc"],
        Stat830xNotSupported: ["NotSupported"],
        Stat830xVerifyError: ["VerifyError"],
        Stat830xI2cBusy: ["I2cBusy"],
        Stat830xI2cNack: ["I2cNack"],
        Stat830xSynthNoLock: ["SynthNoLock"],
        Stat830xFileNotSupported: ["FileNotSupported"],
        Stat830xFlashEraseError: ["FlashEraseError"],
        Stat830xClkMuxCalibError: ["ClkMuxCalibError"],
        Stat830xClkMuxParaBwSelErr: ["ClkMuxParaBwSelErr"],
        Stat830xClkMuxParaN1HsErr: ["ClkMuxParaN1HsErr"],
        Stat830xClkMuxParaNc1LsErr: ["ClkMuxParaNc1LsErr"],
        Stat830xClkMuxParaNc2LsErr: ["ClkMuxParaNc2LsErr"],
        Stat830xClkMuxParaN2HsErr: ["ClkMuxParaN2HsErr"],
        Stat830xClkMuxParaN2LsErr: ["ClkMuxParaN2LsErr"],
        Stat830xClkMuxParaN31Err: ["ClkMuxParaN31Err"],
        Stat830xClkMuxParaClkInFreqErr: ["ClkMuxParaClkInFreqErr"]
    }

    def _interpreter(self, value):
        cdef:
            SIS830X_STATUS code
            char error_str[20]
        if value in self.valid_codes():
            code = value
            sis830x_status2str(code, error_str)
            str_help = error_str[:strlen(error_str)].decode('UTF-8')
            return (self._valid_values[value][0], f"{str_help}")
        return None, f"Unknown {self.__class__.__name__} ({self._code})"
